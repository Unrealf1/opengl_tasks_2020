#include "TreeApplication.hpp"
#include "trees/SimpleTree.hpp"
#include "trees/TestTree.hpp"


int main(int argc, char** argv)
{
    std::cout << "Creating app...\n";
    auto app = TreeApp<SimpleTree>();
    if (argc > 1) {
        auto iters = atoll(argv[1]);
        app.setInitialIterations(iters);
    }
    std::cout << "Starting app..." << std::endl;
    app.start();
    std::cout << "App finished" << std::endl;
}