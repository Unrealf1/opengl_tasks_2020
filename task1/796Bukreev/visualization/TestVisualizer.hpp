#include "TreeVisualizer.hpp"
#include "TreeParts.hpp"


class TestVisualizer : public ITreeVisualizer<glm::vec3, SceneObject> {
public:
    std::vector<SceneObject<glm::vec3>> getParts() override {
        return {{
            SimpleBranch(10.0f, 10.0f).getMesh(),
            glm::mat4(1.0f)
        }};
    }
};