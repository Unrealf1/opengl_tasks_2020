#pragma once

#include "TreeVisualizer.hpp"
#include "../commons.hpp"
#include "TreeParts.hpp"

#include <stack>
#include <tuple>
#include <cmath>

template<typename tree_t>
class SimpleTreeVisualizer : public ITreeVisualizer<glm::vec3, NormedSceneObject> {
    public:
        std::vector<NormedSceneObject<glm::vec3>> getParts() {
            auto graph = tree.getGraph();
            std::vector<NormedSceneObject<glm::vec3>> sceneObjects = {};
            sceneObjects.reserve(graph.numNodes());

            // I know, that graph is actually a tree, so I don't need to remember 
            // visited nodes

            // nodeNumber, starting point, layer number, x/y angle, lastZangle
            std::stack<std::tuple<uint32_t, uint32_t, glm::mat4, float>> toVisit;
            auto initialRotation = glm::rotate(glm::mat4(1.0f), -additionalZangle, glm::vec3(1.0f, 0.0f, 0.0f));
            //auto initialRotation = glm::mat4(1.0f);
            toVisit.push(std::make_tuple(graph.getRoot(), 0, initialRotation, 0.0f));

            while (!toVisit.empty()) {
                auto [current_node, layer, parentMatrix, xyAngle] =  toVisit.top();
                toVisit.pop();

                ++layer;
                float thickness = baseThickness * pow(thicknessDecrease, layer);
                float height = baseHeight * pow(heightDecrease, layer);
                
                auto myRotate = glm::mat4(1.0f);
                myRotate = glm::rotate(myRotate, xyAngle, glm::vec3(0.0f, 0.0f, 1.0f));
                myRotate = glm::rotate(myRotate, additionalZangle, glm::vec3(1.0f, 0.0f, 0.0f));
                
                auto myMatrix = parentMatrix * myRotate;
                
                glm::vec4 top(0.0f, 0.0f, height, 1.0f);
                auto finalTop = myMatrix * top;
                // std::cout << "XYANGLE: " << xyAngle << std::endl;
                // std::cout << "TOP: " << finalTop[0] << ' ' << finalTop[1] << ' ' << finalTop[2] << std::endl;
                // std::cout << "my rotate is\n";
                // for (int i = 0; i < 4; ++i) {
                //     for (int j = 0; j < 4; ++j) {
                //         std::cout << myRotate[i][j] << "    ";
                //     }
                //     std::cout << std::endl;
                // }

                auto childT = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, height));
                auto childMatrix = myMatrix * childT;
                
                auto branch = NormalizedBranch(height, thickness, NormalizedBranch::NormCalcType::smooth);
                auto mesh = branch.getMesh();
                auto norms = branch.getNorms();

                sceneObjects.emplace_back(std::move(mesh), std::move(norms), myMatrix);

                auto children = graph.getBranchesFromNode(current_node);
                auto total_children = children.size();
                
                auto current_xy_angle = 0.0f;
                auto xyAngleStep = 1.5f * M_PI / total_children;
                for (auto& child : children) {
                    toVisit.push(std::make_tuple(child, layer, childMatrix, current_xy_angle));
                    current_xy_angle += xyAngleStep;
                }
            }

            return sceneObjects;
        }

        void iterateTree(uint32_t num_iterations = 1) {
            tree.iteration(num_iterations);
        }

        void setScale(float new_scale) {
            scale = new_scale;
        }

        void setScaleStep(float new_step) {
            scaleStep = new_step;
        }
 
        void doScaleStep() {
            scale += scaleStep;
        }

        void setStartingPoint(const glm::vec3& new_starting_point) {
            bottom_center = new_starting_point;
        }
    private:
        tree_t tree = tree_t();
        float baseThickness = 2.0f;
        float thicknessDecrease = 0.7f;
        float baseHeight = 10.0f;
        float heightDecrease = 0.7f;
        float scale = 1.0f;
        float scaleStep = 0.1f;
        glm::vec3 bottom_center = {0.0f, 0.0f, 0.0f};
        float additionalZangle = M_PI / 10.0;
};