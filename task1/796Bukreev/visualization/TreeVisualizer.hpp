#pragma once

#include <vector>
#include "../commons.hpp"


template<typename Point, template<typename> typename SceneType>
class ITreeVisualizer{
    public:
        virtual std::vector<SceneType<Point>> getParts() = 0;
};