#pragma once

#include <cstdint>
#include "../commons.hpp"
#include <iostream>
#include <cmath>
#include "../geom_math.hpp"
#include <array>
#include <algorithm>


Mesh<glm::vec3> createCone(glm::vec3 bottom_center, float height, float base_width, uint32_t num_planes) {
    if (num_planes < 3) {
        std::cerr << "Cannot create a cone from 2 triangles\n";
        return {};
    }
    glm::vec3 top = {bottom_center[0], bottom_center[1], bottom_center[2] + height};
    Mesh<glm::vec3> result;
    Mesh<glm::vec3> normals;
    result.reserve(num_planes * 2);

    float current_angle = 0.0f;
    float angle_step = 2.0f * M_PI / num_planes;

    std::vector<glm::vec3> points;
    points.reserve(num_planes);
    for (size_t i = 0; i < num_planes; ++i) {
        glm::vec3 current_point = {cos(current_angle) * base_width, sin(current_angle) * base_width, bottom_center[2]};
        current_angle += angle_step;
        points.push_back(current_point);
    }

    for (size_t i = 1; i < points.size(); ++i) {
        auto current_point = points[i];
        auto last_point = points[i-1];
        
        result.push_back(Triangle<glm::vec3> {last_point, current_point, top});
        result.push_back(Triangle<glm::vec3> {last_point, current_point, bottom_center});
    }
    result.push_back(Triangle<glm::vec3> {points.back(), points.front(), top});
    result.push_back(Triangle<glm::vec3> {points.back(), points.front(), bottom_center});
    return result;
}

class SimpleBranch {
    public:
        SimpleBranch(float length, float width): length(length), width(width){}

        virtual Mesh<glm::vec3> getMesh() {
            return createCone({0.0f, 0.0f, 0.0f}, length, width, 30);
        }
    protected:
        float length;
        float width;
};

class NormalizedBranch: SimpleBranch {
public:
    enum class NormCalcType {
        smooth, sharp
    };

    NormalizedBranch(float length, float width, NormCalcType type): SimpleBranch(length, width), normType(type){}

    Mesh<glm::vec3> getMesh() override {
        if (calculatedMesh) { return _mesh; }
        _mesh = SimpleBranch::getMesh();
        calculatedMesh = true;
        return _mesh;
    }

    Mesh<glm::vec3> getNorms() {
        if (calculatedNorm) { return _norm; }
        calcNorm();
        calculatedNorm = true;
        return _norm;
    }

private:
    bool calculatedMesh = false;
    Mesh<glm::vec3> _mesh;

    bool calculatedNorm = false;
    Mesh<glm::vec3> _norm;

    NormCalcType normType;

    void calcNorm() {
        _norm.clear();
        auto mesh = getMesh();
        for (auto& triangle : mesh) {
            Triangle<glm::vec3> new_tr;
            std::transform(triangle.begin(), triangle.end(), new_tr.begin(), [](const glm::vec3& v) {
                if (v == glm::vec3(0.0f, 0.0f, 0.0f)) {
                    return glm::vec3 {0.0f, 0.0f, -1.0f};
                }
                return glm::normalize(v);
            });
            _norm.push_back(new_tr);
        }
    }
};
