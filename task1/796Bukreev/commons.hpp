#pragma once

#include <cstdint>
#include <array>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

/*
My vector space:
X to the right
Y from the screen
Z up
*/

template<typename Point>
using Triangle = std::array<Point, 3>;

template<typename Point>
using Mesh = std::vector<Triangle<Point>>;

template<typename Point>
struct SceneObject {
    SceneObject(Mesh<Point> mesh, glm::mat4 modelMatrix)
    : mesh(mesh), modelMatrix(modelMatrix){}

    Mesh<Point> mesh;
    glm::mat4 modelMatrix;
};

template<typename Point>
struct NormedSceneObject: public SceneObject<Point> {
    NormedSceneObject(Mesh<Point> mesh, Mesh<Point> norms, glm::mat4 modelMatrix): SceneObject<Point>(mesh, modelMatrix), norms(norms){}
    Mesh<Point> norms;
};
