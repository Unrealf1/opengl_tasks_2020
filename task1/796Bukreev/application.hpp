#pragma once

#include "ready/Camera.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>

#include <iostream>
#include <memory>


//Функция обратного вызова для обработки нажатий на клавиатуре
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouseCursosPosCallback(GLFWwindow* window, double xpos, double ypos);
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);

class Application {
public:
    Application():_cameraMover(std::make_shared<OrbitCameraMover>()){}

    virtual void start() {
        initContext();

        initGL();

        makeScene();

        run();
    }

    virtual void handleKey(int key, int scancode, int action, int mods) {
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_ESCAPE)
            {
                glfwSetWindowShouldClose(_window, GL_TRUE);
            }
        }

        _cameraMover->handleKey(_window, key, scancode, action, mods);
    }

    virtual void handleMouseMove(double xpos, double ypos)
    {
        if (ImGui::IsMouseHoveringAnyWindow())
        {
            return;
        }

        _cameraMover->handleMouseMove(_window, xpos, ypos);
    }

    virtual void handleScroll(double xoffset, double yoffset)
    {
        _cameraMover->handleScroll(_window, xoffset, yoffset);
    }


protected:
    virtual void draw() = 0;

    void makeScene() {
        _camera.viewMatrix = glm::lookAt(glm::vec3(0.0f, -5.0f, 10.0f), glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        _camera.projMatrix = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 500.f);
    }

    void initGL() {
        glewExperimental = GL_TRUE;
        glewInit();

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
    }

    void initContext() {
        //Инициализируем библиотеку GLFW
        if (!glfwInit())
        {
            std::cerr << "ERROR: could not start GLFW3\n";
            exit(1);
        }

        //Устанавливаем параметры создания графического контекста
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        //Создаем графический контекст (окно)
        GLFWwindow* window = glfwCreateWindow(800, 600, "Amazing triangle", nullptr, nullptr);
        if (!window)
        {
            std::cerr << "ERROR: could not open window with GLFW3\n";
            glfwTerminate();
            exit(1);
        }

        //Делаем этот контекст текущим
        glfwMakeContextCurrent(window);
        _window = window;

        glfwSwapInterval(0); //Отключаем вертикальную синхронизацию
        glfwSetWindowUserPointer(_window, this); //Регистрируем указатель на данный объект, чтобы потом использовать его в функциях обратного вызова}

        glfwSetKeyCallback(_window, keyCallback); //Регистрирует функцию обратного вызова для обработки событий клавиатуры
        //glfwSetWindowSizeCallback(_window, windowSizeChangedCallback);
        //glfwSetMouseButtonCallback(_window, mouseButtonPressedCallback);
        glfwSetCursorPosCallback(_window, mouseCursosPosCallback);
        glfwSetScrollCallback(_window, scrollCallback);
    }

    virtual void onStop(){};
    virtual void onRun(){};

    void update() {
        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();

        _cameraMover->update(_window, dt);
        _camera = _cameraMover->cameraInfo();
    }

    void run() {
        onRun();
        while (!glfwWindowShouldClose(_window)) //Пока окно не закрыто
        {
            glfwPollEvents(); //Проверяем события ввода

            update(); //Обновляем сцену и положение виртуальной камеры

            draw(); 

            glfwSwapBuffers(_window); //Переключаем передний и задний буферы
        }
        onStop();
    }

    GLFWwindow* _window = nullptr; //Графичекое окно

    CameraInfo _camera;
    CameraMoverPtr _cameraMover;

    //Время на предыдущем кадре
    double _oldTime = 0.0;

};

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    Application* app = (Application*)glfwGetWindowUserPointer(window);
    app->handleKey(key, scancode, action, mods);
}

void mouseCursosPosCallback(GLFWwindow* window, double xpos, double ypos)
{
    Application* app = (Application*)glfwGetWindowUserPointer(window);
    app->handleMouseMove(xpos, ypos);
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    Application* app = (Application*)glfwGetWindowUserPointer(window);
    app->handleScroll(xoffset, yoffset);
}
