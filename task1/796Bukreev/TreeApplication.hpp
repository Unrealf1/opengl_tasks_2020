#pragma once

#include "application.hpp"
#include <vector>

#include "visualization/SimpleTreeVisualizer.hpp"
#include "visualization/TestVisualizer.hpp"

#include "glm/glm.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/transform.hpp"


template<typename tree_t>
class TreeApp : public Application {
public:
    void setInitialIterations(uint32_t initial_iterations) {
        _initial_iterations = initial_iterations;
    }

protected:
    void draw() override {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, width, height);

        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдерную программу
        glUseProgram(_program);

        //Рисуем полигональную модель без индексов
        for (auto& model : _models) {
            auto options = std::get<0>(model);
            auto vertex_count = std::get<1>(model);
            auto& obj = std::get<2>(model);

            glBindVertexArray(options);
            auto model_mat = obj.modelMatrix;           
            auto view_mat = _camera.viewMatrix;
            auto proj_mat = _camera.projMatrix;

            glUniformMatrix4fv(
                glGetUniformLocation(_program, "model"),
                1,
                GL_FALSE,
                glm::value_ptr(model_mat)
            );

            glUniformMatrix4fv(
                glGetUniformLocation(_program, "view"),
                1,
                GL_FALSE,
                glm::value_ptr(view_mat)
            );
            glUniformMatrix4fv(
                glGetUniformLocation(_program, "projection"),
                1,
                GL_FALSE,
                glm::value_ptr(proj_mat)
            );

            glDrawArrays(GL_TRIANGLES, 0, vertex_count);
            glBindVertexArray(0);
        }
        //exit(0);
    }

    GLuint createVertexShader() {
        //Вершинный шейдер
        const char* vertexShaderText = R"(
            #version 330

            layout(location = 0) in vec3 vertexPosition;
            layout(location = 1) in vec3 norm;

            out vec3 colour;

            uniform mat4 model;
            uniform mat4 view;
            uniform mat4 projection;

            void main() {
                gl_Position = projection * view * model * vec4(vertexPosition, 1.0);
                colour = (projection * view * model * vec4(norm, 0.0)).xyz * 0.5 + vec3(0.5);
            }
        )";

        //Создаем шейдерный объект
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        //Передаем в шейдерный объект текст шейдера
        glShaderSource(vertexShader, 1, &vertexShaderText, nullptr);
        //Компилируем шейдер
        glCompileShader(vertexShader);
        //Проверяем ошибки компиляции
        int status = -1;
        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
        if (status != GL_TRUE)
        {
            GLint errorLength;
            glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &errorLength);
            
            std::vector<char> errorMessage;
            errorMessage.resize(errorLength);

            glGetShaderInfoLog(vertexShader, errorLength, 0, errorMessage.data());

            std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;
            
            exit(1);
        }
        return vertexShader;
    }

    GLuint createFragmentShader() {
        //Фрагментный шейдер
        const char* fragmentShaderText = R"(
            #version 330

            in vec3 colour;

            out vec4 fragColor;

            void main() {
                fragColor = vec4(colour, 1.0);
            }
        )";

        //Создаем шейдерный объект
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        //Передаем в шейдерный объект текст шейдера
        glShaderSource(fragmentShader, 1, &fragmentShaderText, nullptr);

        //Компилируем шейдер
        glCompileShader(fragmentShader);

        //Проверяем ошибки компиляции
        int status = -1;
        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
        if (status != GL_TRUE)
        {
            GLint errorLength;
            glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &errorLength);

            std::vector<char> errorMessage;
            errorMessage.resize(errorLength);

            glGetShaderInfoLog(fragmentShader, errorLength, 0, errorMessage.data());

            std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;

            exit(1);
        }
        return fragmentShader;
    }

    GLuint createProgram(GLuint vs, GLuint fs) {
        //Создаем шейдерную программу
        GLuint program = glCreateProgram();

        //Прикрепляем шейдерные объекты
        glAttachShader(program, vs);
        glAttachShader(program, fs);

        //Линкуем программу
        glLinkProgram(program);

        //Проверяем ошибки линковки
        int status = -1;
        glGetProgramiv(program, GL_LINK_STATUS, &status);
        if (status != GL_TRUE)
        {
            GLint errorLength;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &errorLength);

            std::vector<char> errorMessage;
            errorMessage.resize(errorLength);

            glGetProgramInfoLog(program, errorLength, 0, errorMessage.data());

            std::cerr << "Failed to link the program:\n" << errorMessage.data() << std::endl;

            exit(1);
        }
        return program;
    }

    void createTreeCoords() {
        std::cout << "Generating tree...\n";
        SimpleTreeVisualizer<tree_t> visualizer;
        //TestVisualizer visualizer;
        visualizer.iterateTree(_initial_iterations);
        auto objects = visualizer.getParts();

        size_t total_points = 0;
        std::cout << "Tree has " << objects.size() << " branches\n";
        for (auto& obj : objects) {
            std::vector<float> current_coords;
            current_coords.reserve(obj.mesh.size() * 3 * 3);
            total_points += obj.mesh.size() * 3;
            for (auto& triangle : obj.mesh) {
                for (auto& point : triangle) {
                    current_coords.push_back(point[0]);
                    current_coords.push_back(point[1]);
                    current_coords.push_back(point[2]);
                }
            }
            //Создаем буфер VertexBufferObject для хранения координат на видеокарте
            GLuint vbo;
            glGenBuffers(1, &vbo);
            //Делаем этот буфер текущим
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            //Копируем содержимое массива в буфер на видеокарте
            glBufferData(GL_ARRAY_BUFFER, current_coords.size() * sizeof(float), current_coords.data(), GL_STATIC_DRAW);

            GLsizei num_points = current_coords.size()/3;
            GLuint options;
            //Создаем объект VertexArrayObject для хранения настроек полигональной модели
            glGenVertexArrays(1, &options);

            //Делаем этот объект текущим
            glBindVertexArray(options);
            //Делаем буфер с координатами текущим
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            //Включаем 0й вершинный атрибут - координаты
            glEnableVertexAttribArray(0);
            //Устанавливаем настройки:
            //0й атрибут,
            //3 компоненты типа GL_FLOAT,
            //не нужно нормализовать,
            // - расстояние в байтах между 2мя соседними значениями,
            //0 - сдвиг в байтах от начала
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
            //_models.push_back(std::make_tuple(options, num_points, obj));

            std::vector<float> current_norms;
            current_norms.reserve(obj.norms.size() * 3 * 3);
            for (auto& triangle : obj.norms) {
                for (auto& point : triangle) {
                    current_norms.push_back(point[0]);
                    current_norms.push_back(point[1]);
                    current_norms.push_back(point[2]);
                }
            }
            GLuint vbo_norm;
            glGenBuffers(1, &vbo_norm);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_norm);
            glBufferData(GL_ARRAY_BUFFER, current_norms.size() * sizeof(float), current_norms.data(), GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_norm);
            glEnableVertexAttribArray(1);
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
            _models.push_back(std::make_tuple(options, num_points, obj));
            
            glBindVertexArray(0);
        }
        std::cout << "Tree generated with " << total_points << " points\n";
    }

    void onRun() override {
        glDisable(GL_CULL_FACE);

        GLuint vertexShader = createVertexShader();
        GLuint fragmentShader = createFragmentShader();
        _program = createProgram(vertexShader, fragmentShader);
       
        createTreeCoords();

        std::cout << "onRun done" << std::endl;
    }

    void onStop() override {
        //Удаляем созданные объекты OpenGL
        //glDeleteProgram(_program);
        //glDeleteShader(vertexShader);
        //glDeleteShader(fragmentShader);
        // glDeleteVertexArrays(1, &vertexArrayObject);
        // glDeleteBuffers(1, &vertexBufferObject);

        //glfwTerminate();
    }
private:
    std::vector<std::tuple<GLuint, GLsizei, NormedSceneObject<glm::vec3>>> _models;

    GLuint _program;

    glm::vec3 _CameraPosition;
    glm::vec3 _CameraSight = glm::vec3(0.0f, 1.0f, 0.0f);
    float sight_degree = M_PI / 2.0f;
    uint32_t _initial_iterations = 8;
};  