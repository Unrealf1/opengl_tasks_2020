#pragma once

#include "ITree.hpp"

#include <vector>

class TestTree : public ITree {
    public:
        void iteration(int32_t its) override { }

        virtual TreeGraph getGraph() {
            std::cout << "TEST TREE: generating tree graph..." << std::endl;
            std::vector<std::vector<uint32_t>> childrenList(10);
            childrenList[0] = {1, 3, 4};
            childrenList[1] = {2, 5};
            childrenList[5] = {6};


            // std::cout << "TEST TREE: children list:\n";
            // size_t i = 0;
            // for (auto& chlds : childrenList) {
            //     std::cout << i << " | ";
            //     ++i;
            //     for (auto c : chlds) {
            //         std::cout << c << ' ';
            //     }
            //     std::cout << '\n';
            // }
            // std::cout << std::flush;
            return TreeGraph(childrenList);
        }
    private:
        uint32_t num_layers;
};