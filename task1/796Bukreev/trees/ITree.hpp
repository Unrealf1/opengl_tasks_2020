#pragma once

#include <cstdint>
#include <vector>
#include <iostream>


class TreeGraph {
public:
    TreeGraph(std::vector<std::vector<uint32_t>> children): childrenList(children){}

    uint32_t getRoot() { return 0; }

    size_t numNodes() { return childrenList.size(); }

    std::vector<uint32_t> getBranchesFromNode(uint32_t node) const {
        if (node >= childrenList.size()) {
            return {};
        }
        return childrenList[node];
    }
private:
    // childrenList[a] = vector of children of a
    std::vector<std::vector<uint32_t>> childrenList; 
};

class ITree {
public:
    virtual TreeGraph getGraph() = 0;

    virtual void iterOnce() {
        iteration(1);
    }
    virtual void iteration(int32_t) = 0;
    virtual ~ITree() = default;
};