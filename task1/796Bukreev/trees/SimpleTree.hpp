#pragma once

#include "ITree.hpp"

#include <vector>
#include <cmath>

class SimpleTree : public ITree {
    public:
        SimpleTree(uint32_t layers): num_layers(layers){};
        SimpleTree(): SimpleTree(1){};

        void iteration(int32_t its) override {
            if (its <= 0) {return;}
            num_layers += its;
        }

        virtual TreeGraph getGraph() {
            std::cout << "SIMPLE TREE: generating tree graph..." << std::endl;
            std::vector<std::vector<uint32_t>> childrenList(lround(pow(2, num_layers) + 1));
            for (uint32_t layer = 0; layer < num_layers; ++layer) {
                uint32_t start_node = lround(pow(2, layer)) - 1;
                uint32_t end_node = start_node * 2 + 1;
                for (uint32_t node = start_node; node < end_node; ++node) {
                    childrenList[node] = {node * 2 + 1, node * 2 + 2};
                }
            }
            // std::cout << "SIMPLE TREE: children list:\n";
            // size_t i = 0;
            // for (auto& chlds : childrenList) {
            //     std::cout << i << " | ";
            //     ++i;
            //     for (auto c : chlds) {
            //         std::cout << c << ' ';
            //     }
            //     std::cout << '\n';
            // }
            // std::cout << std::flush;
            return TreeGraph(childrenList);
        }
    private:
        uint32_t num_layers;
};